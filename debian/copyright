Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libcpuid
Upstream-Contact: Veselin Georgiev <anrieff@gmail.com>
Source: http://github.com/anrieff/libcpuid
Files-Excluded: contrib debian

Files: *
Copyright: 2008-2015, Veselin Georgiev <anrieff@gmail.com>
License: BSD-2-clause

Files: debian/*
Copyright: 2015, Guodong Zhang <gdzhang@linx-info.com>
  2019, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+
Comment:
 As this project had been packaged outside of Debian long before
 having been uploaded, the GPL-2+ license for debian/* had to be
 kept. Ideally, we re-license the files in debian/* under BSD-2-clause
 just like all the upstream files. This eases upstreaming packaging
 files, if needed, later on.
 .
 Copyright holders of "*" and "debian/*" have been notified on this.

Files: debian/patches/*
Copyright: 2019-2022, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: BSD-2-clause
Comment:
 Assuring that any patches we as Debian maintainers come up with
 are licensed under the same license as the upstream project.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
